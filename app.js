var Client = require('node-rest-client').Client;
var express = require('express');
var app = express();
var http = require("http").Server(app);
var Twitter = require("twitter");
var io = require("socket.io")(http);
var fs = require('fs');

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/img', express.static(__dirname + "/img"));
app.use('/js', express.static(__dirname + "/js"));
app.use('/css', express.static(__dirname + "/css"));
app.use('/media', express.static(__dirname + "/media"));

var twitClient = new Twitter({
  consumer_key: 'VFSZm3HW3au06uEM7QK1Hjoov',
  consumer_secret: 'vrMqmvnTvHHTZuvHZwEU3zACrvK6avYS2lOFwPdD4vwYwaaSCn',
  access_token_key: '29193874-NZ2OS3ciF1B8okEy08GAkOROgKUvcbjpbvgiV9iS6',
  access_token_secret: 'SEogLn3VMXSjdedavcrour8MYDcnFtjvYcK7BQIIhJedQ'
});

twitClient.get('search/tweets', {q: 'BennyBenassi'}, function(error, tweets, response) {
   // console.log(tweets.statuses[0].text);
});

//Setting up Sonos connection
var sonosIp = 'ws://10.11.199.91:1400/websocket/api'; //get this from Sonos folks
var serverIp = 'http://10.11.199.115:3000/queue/v1.0/'; //get this from ifconfig on local machine
var groupId = '';
var householdId = '';

var client = new Client();

var lineage = [
				{
					trackName: 'Bring the Noise Remix',
					blurb: '',
					year: '2007',
					artistName: 'Benny Benassi',
					twitterHandle: 'BennyBenassi',
					songUrl: getMedianetUrl({title:"bring the noise remix",artist:"benni benassi"})
				},
				{
					trackName: 'Bring the Noise',
					blurb: 'The Benny Benassi remix of Public Enemy’s Right The Power slowed the track down, and cut off many of the lyrics. Benassi mixed two versions of the song. The Pump-kin version enxemplifies a heavy melody, while the S-faction edit added more emphasis to the bassline. ',
					year: '1987',
					artistName: 'Public Enemy',
					twitterHandle: 'PublicEnemyFTP',
					songUrl: getMedianetUrl({title:"bring the noise",artist:"public enemy"})
				},
				{
					trackName: 'Funky Drummer',
					blurb: 'Public Enemy sampled James Brown on several different tracks and credits James as being a source of inspiration. The drum break of Funky Drummer is one of the most frequently sampled rhythmic breaks.',
					year: '1970',
					artistName: 'James Brown',
					twitterHandle: '',
					songUrl: getMedianetUrl({title:"funky drummer",artist:"james brown"})
				},
				{
					trackName: 'Lucille',
					blurb: 'James Brown idolized Little Richard and credits him with influencing his departure from gospel music. Little Richard’s Lucille’s  heavy bassline, slower tempo and straight-quaver drum beat, stop-time breaks, heavily influenced rock and r&b. ',
					year: '1957',
					artistName: 'Little Richard',
					twitterHandle: '',
					songUrl: getMedianetUrl({title:"lucille",artist:"little richard"})
				}
			];

	var lineage2 = [
					{
						trackName: 'Ghost',
						blurb: '',
						year: '2016',
						artistName: 'New Thousand',
						twitterHandle: 'newthousandband',
						songSource: 'soundcloud'
					},
					{
						trackName: 'Making of a Cyborg',
						blurb: 'New Thousand’s ghost sampled the taiku drum sounds that appear in several portions of Kenji Kawai’s Making of Cyborg. Making a Cyborg appears on the soundtrack of the movie Ghost in The Shell. ',
						year: '1995',
						artistName: 'Kenji Kawai',
						twitterHandle: ''
					},
					{
						trackName: 'Thunder Drums',
						blurb: 'Kenji Kawai drew inspiration from the traditional taiku drums of Japan.',
						year: '1990',
						artistName: 'Various - Japanese Taiko Drums',
						twitterHandle: ''
					}
				];

var currentLineage = [];
const SonosConnector = require(__dirname + '/js/connector.js'),
    SonosGlobal = require(__dirname + '/js/global.js'),
    SonosGroupVolume = require(__dirname + '/js/groupVolume.js'),
    SonosPlayback = require(__dirname + '/js/playback.js'),
    SonosPlaybackSession = require(__dirname + '/js/playbackSession.js'),
    SonosPlaybackMetadata = require(__dirname + '/js/playbackMetadata.js'),
    connection = new SonosConnector(); // SonosConnector manages the connection to a Sonos group

// playOnSonos();

function playOnSonos(){

	console.log("connecting to sonos device...");
	connection.connect('Sonos_1DCtL34cV8WArsdkPZS90Q7YUf', 'RINCON_949F3E751F3401400:1233757235', sonosIp);
	connection.onConnected = function onConnected() {
	    connection.global = new SonosGlobal(connection); // Receives now playing/up next metadata information
	    connection.playbackSession = new SonosPlaybackSession(connection); // Receives now playing/up next metadata information
	    // connection.metadataStatus = new SonosPlaybackMetadata(connection);
		console.log("connected");
	    connection.global.subscribe(function(msg) {
	    	// console.log('global');
	    	// console.log(JSON.stringify(msg));
	    });
//	    connection.metadataStatus.subscribe(function(msg){
//	    	console.log('metadata: '+msg);
//	    })
		connection.playbackSession.createSession('asdf','asvvdf','asdfas').then(function(psmsg) {
			 console.log('msg:' +JSON.stringify(psmsg));
		    // connection.playbackSession.subscribe(function(msg) {
		    // 	console.log('playback');
		    // 	console.log(JSON.stringify(msg));
		    // });
		    connection.sessionId = psmsg.body.sessionId;
		    console.log('connection val: '+connection.sessionId);
		   	connection.playbackSession.loadCloudQueue(serverIp,'','0',0,true).then(function(cqmsg) {
		    		console.log(cqmsg);
		    	},function(err) {
		    		console.log(err);
		    	});

		});
	}
}

function getMedianetUrl(search){
	var artistClean = search.artist.replace(/ /g,'%20');
	var titleClean = search.title.replace(/ /g,'%20');
	client.get("http://api.mndigital.com?method=Radio.SearchTracks&format=json&title="+titleClean+"&album=&albumMnetId=&artist="+artistClean+"&includeExplicit=True&mainArtistOnly=True&genre=&page=1&pageSize=5&includeKaraoke=False&excludeInterviews=True&apiKey=MN_Hackathon_Key&timestamp=1489585983&signature=ef840c5b0eb4230b51f0ddc0edda34e3", function (data, response) {
	    // parsed response body as js object
	    // console.log("medianet url: ", data.Tracks[0]);
	    for(i in data.Tracks[0].SampleLocations){
	    	if(data.Tracks[0].SampleLocations[i].Type == "p_mp3"){
			    // console.log(data.Tracks[0].SampleLocations[i].Location);
			    for(j in lineage){
			    	if(lineage[j].trackName.toLowerCase() == search.title){
				    	lineage[j].songUrl = data.Tracks[0].SampleLocations[i].Location
				    }
			    }
			    break;
	    	}
	    }
	});
}

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
  // console.log(lineage);
})
app.post('/results',function(req,res){
	// console.log(req.body.search);
	if(req.body.search == "bring the noise" || req.body.search == "bring the noise remix"){
		res.sendFile(__dirname + '/search-results.html');	
		currentLineage = lineage;	
	}else if(req.body.search == 'ghost'){
		res.sendFile(__dirname + '/search-results2.html');
		currentLineage = lineage2;	
	}
	// console.log(currentLineage);
})
app.get('/queue/v1.0/version',function(req,res){
	res.send({'queueVersion':'1.0'});
})
app.get('/queue/v1.0/itemWindow',function(req,res){
	//get music urls
	console.log("songURL: ",currentLineage[3].songUrl)
	res.send({'items':[{
				'track':{
					'mediaUrl': currentLineage[0].songUrl
				},
				'id':'0'
			},
			{
				'track':{
					'mediaUrl': currentLineage[1].songUrl
				},
				'id':'1'
			},
			{
				'track':{
					'mediaUrl': currentLineage[2].songUrl
				},
				'id':'2'
			},
			{
				'track':{
					'mediaUrl': currentLineage[3].songUrl
				},
				'id':'3'
			}],'includesBeggingOfQueue':true,'includesEndOfQueue':true,'queueVersion':'1.0'});
})
http.listen(3000, function(){
	console.log('Listening on port 3000');
})


io.on('connection', function (socket) {
	socket.on('message', function (data){
		if(data = "playSonos"){
			// console.log("Start playing sonos",currentLineage);
			playOnSonos();
		}
  	});
  socket.on('disconnect', function () { });
});
